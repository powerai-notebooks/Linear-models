{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting Started With TensorFlow  \n",
    "\n",
    "This guide begins with a tutorial on TensorFlow Core on PowerAI.  \n",
    "The original tutorial is from /www.tensorflow.org .\n",
    "## Table of Contents\n",
    "1. Tensors\n",
    "2. Importing TensorFlow\n",
    "3. The Computational Graph\n",
    "4. tf.train API\n",
    "5. tf.contrib.learn\n",
    "    * Basic usage\n",
    "    * A custom model\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tensors\n",
    "The central unit of data in TensorFlow is the $tensor$. A tensor consists of a set of primitive values shaped into an array of any number of dimensions. A tensor's $rank$ is its number of dimensions. Here are some examples of tensors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[[1.0, 2.0, 3.0]], [[7.0, 8.0, 9.0]]]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3 # a rank 0 tensor; this is a scalar with shape []\n",
    "[1. ,2., 3.] # a rank 1 tensor; this is a vector with shape [3]\n",
    "[[1., 2., 3.], [4., 5., 6.]] # a rank 2 tensor; a matrix with shape [2, 3]\n",
    "[[[1., 2., 3.]], [[7., 8., 9.]]] # a rank 3 tensor with shape [2, 1, 3]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing TensorFlow\n",
    "\n",
    "The canonical import statement for TensorFlow programs is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import tensorflow as tf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This gives Python access to all of TensorFlow's classes, methods, and symbols. Most of the documentation assumes you have already done this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Computational Graph\n",
    "\n",
    "You might think of TensorFlow Core programs as consisting of two discrete sections:\n",
    "\n",
    "    Building the computational graph.\n",
    "    Running the computational graph.\n",
    "\n",
    "A computational graph is a series of TensorFlow operations arranged into a graph of nodes. Let's build a simple computational graph. Each node takes zero or more tensors as inputs and produces a tensor as an output. One type of node is a constant. Like all TensorFlow constants, it takes no inputs, and it outputs a value it stores internally. We can create two floating point Tensors node1 and node2 as follows:  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "node1 = tf.constant(3.0, tf.float32)\n",
    "node2 = tf.constant(4.0) # also tf.float32 implicitly"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The final print statement produces"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tensor(\"Const:0\", shape=(), dtype=float32) Tensor(\"Const_1:0\", shape=(), dtype=float32)\n"
     ]
    }
   ],
   "source": [
    "print node1, node2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice that printing the nodes does not output the values 3.0 and 4.0 as you might expect. Instead, they are nodes that, when evaluated, would produce 3.0 and 4.0, respectively. To actually evaluate the nodes, we must run the computational graph within a $session$. A session encapsulates the control and state of the TensorFlow runtime."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code creates a $Session$ object and then invokes its $run$ method to run enough of the computational graph to evaluate $node1$ and $node2$. By running the computational graph in a session as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[3.0, 4.0]\n"
     ]
    }
   ],
   "source": [
    "sess = tf.Session()\n",
    "print(sess.run([node1, node2]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "we see the expected values of 3.0 and 4.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can build more complicated computations by combining $Tensor$ nodes with operations (Operations are also nodes.). For example, we can add our two constant nodes and produce a new graph as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "node3:  Tensor(\"Add:0\", shape=(), dtype=float32)\n",
      "sess.run(node3):  7.0\n"
     ]
    }
   ],
   "source": [
    "node3 = tf.add(node1, node2)\n",
    "\n",
    "print \"node3: \", node3\n",
    "print \"sess.run(node3): \",sess.run(node3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "TensorFlow provides a utility called TensorBoard that can display a picture of the computational graph. Here is a screenshot showing how TensorBoard visualizes the graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "writer =tf.summary.FileWriter('./tensorboard/Getting Started With TensorFlow/graph1', sess.graph)\n",
    "writer.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To open the graph: tensorboard --logdir=path/to/log-directory  in terminal  \n",
    "where logdir points to the directory where the FileWriter serialized its data. If this logdir directory contains subdirectories which contain serialized data from separate runs, then TensorBoard will visualize the data from all of those runs. Once TensorBoard is running, navigate your web browser to localhost:6006 to view the TensorBoard.\n",
    "\n",
    "When looking at TensorBoard, you will see the navigation tabs in the top right corner. Each tab represents a set of serialized data that can be visualized.\n",
    "\n",
    "Here we use:   \n",
    "   /opt/DL/tensorflow/bin/tensorboard --port=8888 --logdir=\"./tensorboard/Getting Started With TensorFlow/graph1\"  \n",
    "Then login TensorBorad from browser"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "tf.reset_default_graph()  # resetting graph"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last two print statements produce\n",
    "\n",
    "As it stands, this graph is not especially interesting because it always produces a constant result. A graph can be parameterized to accept external inputs, known as placeholders. A placeholder is a promise to provide a value later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "tf.reset_default_graph()  # resetting graph\n",
    "a = tf.placeholder(\"float\") # Create a symbolic variable 'a'\n",
    "b = tf.placeholder(\"float\") # Create a symbolic variable 'b'\n",
    "\n",
    "adder_node = a + b  # + provides a shortcut for tf.add(a, b)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The preceding three lines are a bit like a function or a lambda in which we define two input parameters (a and b) and then an operation on them. We can evaluate this graph with multiple inputs by using the feed_dict parameter to specify Tensors that provide concrete values to these placeholders:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "3.000000 should equal 2.0\n",
      "6.000000 should equal 9.0\n"
     ]
    }
   ],
   "source": [
    "sess = tf.Session()\n",
    "print(\"%f should equal 2.0\" % sess.run(adder_node, feed_dict={a: 1, b: 2})) # eval expressions with parameters for a and b\n",
    "print(\"%f should equal 9.0\" % sess.run(adder_node, feed_dict={a: 3, b: 3}))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can make the computational graph more complex by adding another operation. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "add_and_triple = adder_node * 3."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "produces the output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "22.5\n"
     ]
    }
   ],
   "source": [
    "print(sess.run(add_and_triple, {a: 3, b:4.5}))\n",
    "writer2 = tf.summary.FileWriter('./tensorboard/Getting Started With TensorFlow/graph2', sess.graph)\n",
    "writer2.close()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "run this line in terminal:\n",
    "/opt/DL/tensorflow/bin/tensorboard --port=8888 --logdir=\"./tensorboard/Getting Started With TensorFlow/graph2\"  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In machine learning we will typically want a model that can take arbitrary inputs, such as the one above. To make the model trainable, we need to be able to modify the graph to get new outputs with the same input. $Variables$ allow us to add trainable parameters to a graph. They are constructed with a type and initial value:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#tf.reset_default_graph()  # resetting graph\n",
    "\n",
    "W = tf.Variable([.3], tf.float32)\n",
    "b = tf.Variable([-.3], tf.float32)\n",
    "x = tf.placeholder(tf.float32)\n",
    "linear_model = W * x + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Constants are initialized when you call $tf.constant$, and their value can never change. By contrast, variables are not initialized when you call $tf.Variable$. To initialize all the variables in a TensorFlow program, you must explicitly call a special operation as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "init = tf.global_variables_initializer()\n",
    "sess.run(init)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is important to realize $init$ is a handle to the TensorFlow sub-graph that initializes all the global variables. Until we call $sess.run$, the variables are uninitialized.\n",
    "\n",
    "Since $x$ is a placeholder, we can evaluate $linear_model$ for several values of x simultaneously as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 0.          0.30000001  0.60000002  0.90000004]\n"
     ]
    }
   ],
   "source": [
    "print(sess.run(linear_model, {x:[1,2,3,4]}))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We've created a model, but we don't know how good it is yet. To evaluate the model on training data, we need a $y$ placeholder to provide the desired values, and we need to write a loss function.\n",
    "\n",
    "A loss function measures how far apart the current model is from the provided data. We'll use a standard loss model for linear regression, which sums the squares of the deltas between the current model and the provided data. $linear_model - y$ creates a vector where each element is the corresponding example's error delta. We call $tf.square$ to square that error. Then, we sum all the squared errors to create a single scalar that abstracts the error of all examples using $tf.reduce\\_sum$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "23.66\n"
     ]
    }
   ],
   "source": [
    "y = tf.placeholder(tf.float32)\n",
    "squared_deltas = tf.square(linear_model - y)\n",
    "loss = tf.reduce_sum(squared_deltas)\n",
    "print(sess.run(loss, {x:[1,2,3,4], y:[0,-1,-2,-3]}))\n",
    "writer3 = tf.summary.FileWriter('./tensorboard/Getting Started With TensorFlow/graph3', sess.graph)\n",
    "writer3.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We could improve this manually by reassigning the values of $W$ and $b$ to the perfect values of -1 and 1. A variable is initialized to the value provided to $tf.Variable$ but can be changed using operations like $tf.assign$. For example, $W=-1$ and $b=1$ are the optimal parameters for our model. We can change W and b accordingly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "0.0\n"
     ]
    }
   ],
   "source": [
    "fixW = tf.assign(W, [-1.])\n",
    "fixb = tf.assign(b, [1.])\n",
    "sess.run([fixW, fixb])\n",
    "print(sess.run(loss, {x:[1,2,3,4], y:[0,-1,-2,-3]}))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## tf.train API"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A complete discussion of machine learning is out of the scope of this tutorial. However, TensorFlow provides $optimizers$ that slowly change each variable in order to minimize the loss function. The simplest optimizer is $gradient descent$. It modifies each variable according to the magnitude of the derivative of loss with respect to that variable. In general, computing symbolic derivatives manually is tedious and error-prone. Consequently, TensorFlow can automatically produce derivatives given only a description of the model using the function $tf.gradients$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "W: [-0.9999969] b: [ 0.99999082] loss: 5.69997e-11\n"
     ]
    }
   ],
   "source": [
    "tf.reset_default_graph()  # resetting graph\n",
    "\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "\n",
    "# Model parameters\n",
    "W = tf.Variable([.3], tf.float32)\n",
    "b = tf.Variable([-.3], tf.float32)\n",
    "# Model input and output\n",
    "x = tf.placeholder(tf.float32)\n",
    "linear_model = W * x + b\n",
    "y = tf.placeholder(tf.float32)\n",
    "# loss\n",
    "loss = tf.reduce_sum(tf.square(linear_model - y)) # sum of the squares\n",
    "# optimizer\n",
    "optimizer = tf.train.GradientDescentOptimizer(0.01)\n",
    "train = optimizer.minimize(loss)\n",
    "# training data\n",
    "x_train = [1,2,3,4]\n",
    "y_train = [0,-1,-2,-3]\n",
    "# training loop\n",
    "init = tf.global_variables_initializer()\n",
    "sess = tf.Session()\n",
    "sess.run(init) # reset values to wrong\n",
    "for i in range(1000):\n",
    "    sess.run(train, {x:x_train, y:y_train})\n",
    "\n",
    "# evaluate training accuracy\n",
    "curr_W, curr_b, curr_loss  = sess.run([W, b, loss], {x:x_train, y:y_train})\n",
    "print(\"W: %s b: %s loss: %s\"%(curr_W, curr_b, curr_loss))\n",
    "writer4 = tf.summary.FileWriter('./tensorboard/Getting Started With TensorFlow/graph4', sess.graph)\n",
    "writer4.close()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This more complicated program can still be visualized in TensorBoard   \n",
    "run this line in terminal:  \n",
    "    /opt/DL/tensorflow/bin/tensorboard --port=8888 --logdir=\"./tensorboard/Getting Started With TensorFlow/graph4/\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## tf.contrib.learn  \n",
    "$tf.contrib.learn$ is a high-level TensorFlow library that simplifies the mechanics of machine learning, including the following:\n",
    "\n",
    "    running training loops\n",
    "    running evaluation loops\n",
    "    managing data sets\n",
    "    managing feeding\n",
    "\n",
    "tf.contrib.learn defines many common models.\n",
    "### Basic usage\n",
    "Notice how much simpler the linear regression program becomes with tf.contrib.learn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Using config: {'_save_checkpoints_secs': 1, '_num_ps_replicas': 0, '_keep_checkpoint_max': 5, '_tf_random_seed': None, '_task_type': None, '_environment': 'local', '_is_chief': True, '_cluster_spec': <tensorflow.python.training.server_lib.ClusterSpec object at 0x14007bcc5150>, '_tf_config': gpu_options {\n",
      "  per_process_gpu_memory_fraction: 1\n",
      "}\n",
      ", '_task_id': 0, '_save_summary_steps': 100, '_save_checkpoints_steps': None, '_evaluation_master': '', '_keep_checkpoint_every_n_hours': 10000, '_master': ''}\n",
      "WARNING:tensorflow:Rank of input Tensor (1) should be the same as output_rank (2) for column. Will attempt to expand dims. It is highly recommended that you resize your input, as this behavior may change.\n",
      "WARNING:tensorflow:From /opt/DL/tensorflow/lib/python2.7/site-packages/tensorflow/contrib/learn/python/learn/estimators/head.py:1362: scalar_summary (from tensorflow.python.ops.logging_ops) is deprecated and will be removed after 2016-11-30.\n",
      "Instructions for updating:\n",
      "Please switch to tf.summary.scalar. Note that tf.summary.scalar uses the node name instead of the tag. This means that TensorFlow will automatically de-duplicate summary names based on the scope they are created in. Also, passing a tensor or list of tags to a scalar summary op is no longer supported.\n",
      "INFO:tensorflow:Create CheckpointSaverHook.\n",
      "INFO:tensorflow:Saving checkpoints for 2 into ./tensorboard/linear_model/model.ckpt.\n",
      "INFO:tensorflow:loss = 1.44126, step = 2\n",
      "INFO:tensorflow:global_step/sec: 741.862\n",
      "INFO:tensorflow:loss = 0.0903229, step = 102\n",
      "INFO:tensorflow:global_step/sec: 817.74\n",
      "INFO:tensorflow:loss = 0.0355188, step = 202\n",
      "INFO:tensorflow:global_step/sec: 817.028\n",
      "INFO:tensorflow:loss = 0.0045337, step = 302\n",
      "INFO:tensorflow:global_step/sec: 824.279\n",
      "INFO:tensorflow:loss = 0.000739253, step = 402\n",
      "INFO:tensorflow:global_step/sec: 821.39\n",
      "INFO:tensorflow:loss = 0.000309224, step = 502\n",
      "INFO:tensorflow:Saving checkpoints for 551 into ./tensorboard/linear_model/model.ckpt.\n",
      "INFO:tensorflow:global_step/sec: 270.191\n",
      "INFO:tensorflow:loss = 0.00016754, step = 602\n",
      "INFO:tensorflow:global_step/sec: 776.989\n",
      "INFO:tensorflow:loss = 3.11003e-06, step = 702\n",
      "INFO:tensorflow:global_step/sec: 751.688\n",
      "INFO:tensorflow:loss = 7.86577e-06, step = 802\n",
      "INFO:tensorflow:global_step/sec: 848.133\n",
      "INFO:tensorflow:loss = 1.00856e-06, step = 902\n",
      "INFO:tensorflow:Saving checkpoints for 1001 into ./tensorboard/linear_model/model.ckpt.\n",
      "INFO:tensorflow:Loss for final step: 3.12714e-07.\n",
      "WARNING:tensorflow:Rank of input Tensor (1) should be the same as output_rank (2) for column. Will attempt to expand dims. It is highly recommended that you resize your input, as this behavior may change.\n",
      "WARNING:tensorflow:From /opt/DL/tensorflow/lib/python2.7/site-packages/tensorflow/contrib/learn/python/learn/estimators/head.py:1362: scalar_summary (from tensorflow.python.ops.logging_ops) is deprecated and will be removed after 2016-11-30.\n",
      "Instructions for updating:\n",
      "Please switch to tf.summary.scalar. Note that tf.summary.scalar uses the node name instead of the tag. This means that TensorFlow will automatically de-duplicate summary names based on the scope they are created in. Also, passing a tensor or list of tags to a scalar summary op is no longer supported.\n",
      "INFO:tensorflow:Starting evaluation at 2017-06-10-23:07:22\n",
      "INFO:tensorflow:Finished evaluation at 2017-06-10-23:07:23\n",
      "INFO:tensorflow:Saving dict for global step 1001: global_step = 1001, loss = 4.0257e-07\n",
      "WARNING:tensorflow:Skipping summary for global_step, must be a float or np.float32.\n",
      "{'loss': 4.0257001e-07, 'global_step': 1001}\n"
     ]
    }
   ],
   "source": [
    "tf.reset_default_graph()  # resetting graph\n",
    "\n",
    "\n",
    "import tensorflow as tf\n",
    "# NumPy is often used to load, manipulate and preprocess data.\n",
    "import numpy as np\n",
    "\n",
    "# Declare list of features. We only have one real-valued feature. There are many\n",
    "# other types of columns that are more complicated and useful.\n",
    "features = [tf.contrib.layers.real_valued_column(\"x\", dimension=1)]\n",
    "\n",
    "# An estimator is the front end to invoke training (fitting) and evaluation\n",
    "# (inference). There are many predefined types like linear regression,\n",
    "# logistic regression, linear classification, logistic classification, and\n",
    "# many neural network classifiers and regressors. The following code\n",
    "# provides an estimator that does linear regression.\n",
    "estimator = tf.contrib.learn.LinearRegressor(feature_columns=features,\n",
    "                                            model_dir=\"./tensorboard/linear_model\",\n",
    "    config=tf.contrib.learn.RunConfig(save_checkpoints_secs=1))\n",
    "\n",
    "# TensorFlow provides many helper methods to read and set up data sets.\n",
    "# Here we use `numpy_input_fn`. We have to tell the function how many batches\n",
    "# of data (num_epochs) we want and how big each batch should be.\n",
    "x = np.array([1., 2., 3., 4.])\n",
    "y = np.array([0., -1., -2., -3.])\n",
    "input_fn = tf.contrib.learn.io.numpy_input_fn({\"x\":x}, y, batch_size=4,\n",
    "                                              num_epochs=1000)\n",
    "\n",
    "# We can invoke 1000 training steps by invoking the `fit` method and passing the\n",
    "# training data set.\n",
    "estimator.fit(input_fn=input_fn, steps=1000)\n",
    "\n",
    "# Here we evaluate how well our model did. In a real example, we would want\n",
    "# to use a separate validation and testing data set to avoid overfitting.\n",
    "print(estimator.evaluate(input_fn=input_fn))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This more complicated program can still be visualized in TensorBoard   \n",
    "run this line in terminal:  \n",
    "    /opt/DL/tensorflow/bin/tensorboard --port=8888 --logdir=\"./tensorboard/Getting Started With TensorFlow/linear_model/\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A custom model\n",
    "\n",
    "$tf.contrib.learn$ does not lock you into its predefined models. Suppose we wanted to create a custom model that is not built into TensorFlow. We can still retain the high level abstraction of data set, feeding, training, etc. of $tf.contrib.learn$. For illustration, we will show how to implement our own equivalent model to $LinearRegressor$ using our knowledge of the lower level TensorFlow API.\n",
    "\n",
    "To define a custom model that works with $tf.contrib.learn$, we need to use $tf.contrib.learn.Estimator$. $tf.contrib.learn.LinearRegressor$ is actually a sub-class of tf.contrib.learn.Estimator. Instead of sub-classing Estimator, we simply provide $Estimator$ a function $model\\_fn$ that tells $tf.contrib.learn$ how it can evaluate predictions, training steps, and loss. The code is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "INFO:tensorflow:Using config: {'_save_checkpoints_secs': 1, '_num_ps_replicas': 0, '_keep_checkpoint_max': 5, '_tf_random_seed': None, '_task_type': None, '_environment': 'local', '_is_chief': True, '_cluster_spec': <tensorflow.python.training.server_lib.ClusterSpec object at 0x1400ccca6450>, '_tf_config': gpu_options {\n",
      "  per_process_gpu_memory_fraction: 1\n",
      "}\n",
      ", '_task_id': 0, '_save_summary_steps': 100, '_save_checkpoints_steps': None, '_evaluation_master': '', '_keep_checkpoint_every_n_hours': 10000, '_master': ''}\n",
      "INFO:tensorflow:Create CheckpointSaverHook.\n",
      "INFO:tensorflow:Saving checkpoints for 1 into ./tensorboard/linear_model2/model.ckpt.\n",
      "INFO:tensorflow:loss = 50.0922569395, step = 1\n",
      "INFO:tensorflow:global_step/sec: 955.629\n",
      "INFO:tensorflow:loss = 0.130620803382, step = 101\n",
      "INFO:tensorflow:global_step/sec: 1291.87\n",
      "INFO:tensorflow:loss = 0.00797330205759, step = 201\n",
      "INFO:tensorflow:global_step/sec: 1281.02\n",
      "INFO:tensorflow:loss = 0.00104528974141, step = 301\n",
      "INFO:tensorflow:global_step/sec: 1220.54\n",
      "INFO:tensorflow:loss = 2.09113703454e-05, step = 401\n",
      "INFO:tensorflow:global_step/sec: 1290.67\n",
      "INFO:tensorflow:loss = 5.51567645661e-06, step = 501\n",
      "INFO:tensorflow:global_step/sec: 1295.71\n",
      "INFO:tensorflow:loss = 5.07278223059e-07, step = 601\n",
      "INFO:tensorflow:global_step/sec: 1265.84\n",
      "INFO:tensorflow:loss = 3.18386072775e-08, step = 701\n",
      "INFO:tensorflow:global_step/sec: 1305.36\n",
      "INFO:tensorflow:loss = 6.7841530701e-09, step = 801\n",
      "INFO:tensorflow:Saving checkpoints for 852 into ./tensorboard/linear_model2/model.ckpt.\n",
      "INFO:tensorflow:global_step/sec: 379.321\n",
      "INFO:tensorflow:loss = 3.18305882188e-10, step = 901\n",
      "INFO:tensorflow:Saving checkpoints for 1000 into ./tensorboard/linear_model2/model.ckpt.\n",
      "INFO:tensorflow:Loss for final step: 5.10667779873e-12.\n",
      "INFO:tensorflow:Starting evaluation at 2017-06-10-23:07:26\n",
      "INFO:tensorflow:Evaluation [1/10]\n",
      "INFO:tensorflow:Evaluation [2/10]\n",
      "INFO:tensorflow:Evaluation [3/10]\n",
      "INFO:tensorflow:Evaluation [4/10]\n",
      "INFO:tensorflow:Evaluation [5/10]\n",
      "INFO:tensorflow:Evaluation [6/10]\n",
      "INFO:tensorflow:Evaluation [7/10]\n",
      "INFO:tensorflow:Evaluation [8/10]\n",
      "INFO:tensorflow:Evaluation [9/10]\n",
      "INFO:tensorflow:Evaluation [10/10]\n",
      "INFO:tensorflow:Finished evaluation at 2017-06-10-23:07:26\n",
      "INFO:tensorflow:Saving dict for global step 1000: global_step = 1000, loss = 3.8609e-11\n",
      "WARNING:tensorflow:Skipping summary for global_step, must be a float or np.float32.\n",
      "{'loss': 3.8608994e-11, 'global_step': 1000}\n"
     ]
    }
   ],
   "source": [
    "tf.reset_default_graph()  # resetting graph\n",
    "\n",
    "\n",
    "import numpy as np\n",
    "import tensorflow as tf\n",
    "# Declare list of features, we only have one real-valued feature\n",
    "def model(features, labels, mode):\n",
    "    # Build a linear model and predict values\n",
    "    W = tf.get_variable(\"W\", [1], dtype=tf.float64)\n",
    "    b = tf.get_variable(\"b\", [1], dtype=tf.float64)\n",
    "    y = W*features['x'] + b\n",
    "    # Loss sub-graph\n",
    "    loss = tf.reduce_sum(tf.square(y - labels))\n",
    "    # Training sub-graph\n",
    "    global_step = tf.train.get_global_step()\n",
    "    optimizer = tf.train.GradientDescentOptimizer(0.01)\n",
    "    train = tf.group(optimizer.minimize(loss),\n",
    "                   tf.assign_add(global_step, 1))\n",
    "    # ModelFnOps connects subgraphs we built to the\n",
    "    # appropriate functionality.\n",
    "    return tf.contrib.learn.ModelFnOps(\n",
    "        mode=mode, predictions=y,\n",
    "        loss=loss,\n",
    "        train_op=train)\n",
    "\n",
    "estimator = tf.contrib.learn.Estimator(model_fn=model, model_dir=\"./tensorboard/linear_model2\",\n",
    "    config=tf.contrib.learn.RunConfig(save_checkpoints_secs=1))\n",
    "# define our data set\n",
    "x = np.array([1., 2., 3., 4.])\n",
    "y = np.array([0., -1., -2., -3.])\n",
    "input_fn = tf.contrib.learn.io.numpy_input_fn({\"x\": x}, y, 4, num_epochs=1000)\n",
    "\n",
    "# train\n",
    "estimator.fit(input_fn=input_fn, steps=1000)\n",
    "# evaluate our model\n",
    "print(estimator.evaluate(input_fn=input_fn, steps=10))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This more complicated program can still be visualized in TensorBoard   \n",
    "run this line in terminal:  \n",
    "    /opt/DL/tensorflow/bin/tensorboard --port=8888 --logdir=\"./tensorboard/Getting Started With TensorFlow/linear_model2/\""
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
